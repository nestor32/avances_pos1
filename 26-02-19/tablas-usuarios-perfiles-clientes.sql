use proyectoPOS;


create table Perfiles( Id_perfil int auto_increment primary key,
Perfil varchar (20));

create table Cliente (Id_Cliente int auto_increment primary key,
Nombre varchar(20), A_Paterno varchar (20), 
A_Materno varchar (20), fecha date, monto float );

create table Usuario (id_Usuario int auto_increment primary key,
Nombre varchar(20), A_Paterno varchar(20),
A_Materno varchar (20), telefono varchar (15), correo varchar (20), 
funcion varchar(20), usario varchar(20), password varchar (15));
